# Python Job Finder


## Usage

Needed: Docker, Docker Compose

You also need to have the `docker-compose.yml` file from this repository locally.

`docker-compose run django ./manage.py migrate`

`docker-compose run django ./manage.py createsuperuser`

`docker-compose up django`

`docker-compose run scrapy`


## Configuration

Preferably done by the `.env` file, containing:

```env
POSTGRES_DB=jobs
POSTGRES_PASSWORD=<pg_pass>

DATABASE_URL=postgresql://postgres:<pg_pass>@db/jobs
SECRET_KEY=<some_secret_key>
SENTRY_DSN=https://<sentry_key>@sentry.io/<sentry_project>
```


## Development

Needed extra: Python 3

Preferably under `virtualenv`:

`pip install pip-tools` (once)

`pip-sync requirements*.txt` (keeping the PyPI dependencies up-to-date)

`./test.sh`

`scrapy crawl <spider>`, e.g. `scrapy crawl remoteok`.
`./scrape_all.sh` to scrape all sites.

[Scrapy shell](https://doc.scrapy.org/en/latest/topics/shell.html)

`./manage.py runserver [PORT]`

`./manage.py makemigrations`

`./manage.py migrate`

`docker-compose build`
