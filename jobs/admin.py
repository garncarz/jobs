from django.contrib import admin

from . import models


class JobAdmin(admin.ModelAdmin):

    list_display = ['name', 'company', 'posted']
    ordering = ['-posted']


admin.site.register(models.Job, JobAdmin)
