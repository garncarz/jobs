from django.db import models
from django.contrib.postgres import fields as pg_fields
from django.urls import reverse


class Job(models.Model):

    class Meta:
        ordering = ['-posted']

    id = models.CharField(primary_key=True, max_length=50)

    name = models.CharField(max_length=150)
    url = models.URLField()

    posted = models.DateField(null=True)
    location = models.CharField(max_length=100, null=True)

    tags = pg_fields.ArrayField(models.CharField(max_length=40), null=True)

    company = models.CharField(max_length=100)
    contact = models.CharField(max_length=200, null=True)

    text = models.TextField(null=True)

    def get_absolute_url(self):
        return reverse('job-detail', kwargs={'pk': self.pk})
