from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.JobListView.as_view(), name='jobs-list'),
    url(r'^(?P<pk>[\w]+)/$', views.JobDetailView.as_view(), name='job-detail'),
]
