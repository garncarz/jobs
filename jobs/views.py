from django.views.generic import ListView, DetailView

from . import models


class JobListView(ListView):

    model = models.Job


class JobDetailView(DetailView):

    model = models.Job
