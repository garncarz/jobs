import scrapy

from .. import utils


class RemoteOkSpider(scrapy.Spider):
    name = 'remoteok'
    allowed_domains = ['remoteok.io']
    start_urls = ['http://remoteok.io/remote-python-jobs']

    def parse(self, response):
        for job in response.css('#jobsboard .job'):
            yield scrapy.Request(
                url=response.urljoin(
                    job.css('a.preventLink ::attr(href)').extract_first()
                ),
                callback=self.parse_details,
            )

    def parse_details(self, response):
        job = response

        yield dict(
            id=job.css('::attr(data-id)').extract_first(),

            name=job.css('.position h2 ::text').extract_first(),
            url=response.url,

            posted=utils.rel2abs_time(
                job.css('.time ::text').extract_first()
            ),

            tags=job.css('.tags .tag h3 ::text').re(r'(.*)3>$'),

            company=dict(
                name=job.css('.companyLink h3 ::text').extract_first(),
                url=response.urljoin(
                    job.css('.companyLink ::attr(href)').extract_first()
                ),
            ),

            text=job.css('.description').extract_first(),
        )
