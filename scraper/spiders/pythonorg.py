import re

import dateutil.parser
import scrapy


class PythonOrgSpider(scrapy.Spider):
    name = 'pythonorg'
    allowed_domains = ['python.org']
    start_urls = ['https://www.python.org/jobs/location/telecommute/']

    def parse(self, response):
        for job in response.css('.list-recent-jobs li'):
            yield scrapy.Request(
                url=response.urljoin(
                    job.css('a ::attr(href)').extract_first()
                ),
                callback=self.parse_details,
            )

    def parse_details(self, response):
        job = response
        name = job.css('.company-name ::text').extract()

        yield dict(
            id=re.search(r'/(\d+)/', response.url).group(1),

            name=name[0].strip(),
            url=response.url,

            posted=dateutil.parser.parse(
                job.css('time ::attr(datetime)').extract_first()
            ),
            location=job.css('.listing-location a ::text').extract_first(),

            tags=job.css('.listing-job-type a ::text').extract(),

            company=dict(
                name=name[1].strip(),
                url=job.xpath('//strong[contains(., "Web")]'
                              '/following-sibling::a/@href').extract_first(),
            ),

            contact=dict(
                name=job.xpath('//strong[contains(., "Contact")]'
                               '/../text()').re_first(r': (.*)'),
                email=''.join(
                    job.xpath('//strong[contains(., "E-mail")]'
                              '/following-sibling::a')
                       .css('::text').extract()
                ),
            ),

            text=job.css('.job-description').extract_first(),
        )
