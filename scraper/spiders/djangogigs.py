import dateutil.parser
import scrapy


class DjangoGigsSpider(scrapy.Spider):
    name = 'djangogigs'
    allowed_domains = ['djangogigs.com']
    start_urls = ['https://djangogigs.com/gigs/remote/']

    def parse(self, response):
        for job in response.css('#list-content .row'):
            yield dict(
                id=job.css('a ::attr(href)').re_first(r'/(\d+)/'),

                name=job.css('.listing-title ::text').extract_first(),
                url=response.urljoin(
                    job.css('a ::attr(href)').extract_first()
                ),

                posted=dateutil.parser.parse(
                    job.xpath('../preceding-sibling::h5')[-1]
                       .css('::text').extract_first()
                ),
                location=job.css('.co')
                            .xpath('./following-sibling::span')
                            .css('::text').extract_first(),

                company=dict(
                    name=job.css('.co ::text').extract_first(),
                ),

                text=job.css('.listing-content').extract_first(),
            )

        next_page = response.css('.pagination li')[2] \
                            .css('a ::attr(href)').extract_first()
        if next_page and next_page != '#':
            yield scrapy.Request(
                url=response.urljoin(next_page),
                callback=self.parse,
            )
