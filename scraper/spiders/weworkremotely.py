from datetime import datetime
import hashlib

import dateutil.parser
from dateutil.relativedelta import relativedelta
import scrapy


class WeWorkRemotelySpider(scrapy.Spider):
    name = 'weworkremotely'
    allowed_domains = ['weworkremotely.com']
    start_urls = ['https://weworkremotely.com/remote-jobs/search?term=python']

    def parse(self, response):
        for job in response.css('#job_list li'):
            if job.css('a[class="view-all"]'):
                continue

            yield scrapy.Request(
                url=response.urljoin(
                    job.css('a ::attr(href)').extract()[-1]
                ),
                callback=self.parse_details,
            )

    def parse_details(self, response):
        job = response

        posted = dateutil.parser.parse(
            job.css('.listing-header-container h3 time')
               .xpath('@datetime')
               .extract_first(),
            ignoretz=True,
        )
        if posted > datetime.now():
            posted -= relativedelta(years=1)

        yield dict(
            id=hashlib.md5(response.url.encode()).hexdigest(),
            # unfortunately no integer id provided anymore

            name=job.css('.content h1 ::text').extract_first(),
            url=response.url,

            posted=posted,
            location=job.css('.location ::text')
                        .re_first(r'(?:[^:]*: )?(.*)'),

            company=dict(
                name=job.css('.company ::text').extract_first(),
            ),

            text=job.css('.job').extract_first(),
        )
