from jobs import models


class IdPipeline:

    def process_item(self, item, spider):
        item['id'] = '%s%s' % (spider.name, item['id'])
        return item


class DBPipeline:

    def process_item(self, item, spider):
        job, created = models.Job.objects.get_or_create(id=item['id'])

        for attr in ['name', 'url', 'posted', 'location', 'tags', 'text']:
            try:
                setattr(job, attr, item.get(attr))
            except Exception:
                pass

        try:
            job.company = '{name}'.format(**item['company'])
        except Exception:
            pass

        try:
            job.contact = '{name} ({email})'.format(**item['contact'])
        except Exception:
            pass

        job.save()

        return item
