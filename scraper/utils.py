from datetime import datetime, timedelta
import re


def rel2abs_time(reltime):
    m = re.search(r'(\d+)d', reltime)
    days = int(m.group(1)) if m else 0

    m = re.search(r'(\d+)mo', reltime)
    months = int(m.group(1)) if m else 0

    m = re.search(r'(\d+)yr', reltime)
    years = int(m.group(1)) if m else 0

    time_diff = timedelta(days=days + 30*months + 365*years)
    abstime = datetime.now() - time_diff
    abstime = abstime.replace(hour=0, minute=0, second=0, microsecond=0)

    return abstime
