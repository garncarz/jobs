from datetime import datetime, timedelta

from scraper import utils


def test_rel2abs_time():
    def assert_timedelta(str_td, td_days):
        assert timedelta(days=0) \
               < datetime.now() - utils.rel2abs_time(str_td) - timedelta(days=td_days) \
               < timedelta(days=1)

    assert_timedelta('12d', 12)
    assert_timedelta('3mo', 3 * 30)
    assert_timedelta('1yr', 365)

    assert_timedelta('12m', 0)  # minutes
    assert_timedelta('5h', 0)  # hours
