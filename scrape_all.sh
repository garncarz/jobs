#!/usr/bin/env bash

for scraper in `scrapy list`;
do
    scrapy crawl $scraper
done
